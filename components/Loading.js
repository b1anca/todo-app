import * as React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'

const Loading = ({ size }) => {
  return (
    <Container>
      <ActivityIndicator size={size} />
    </Container>
  )
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

Loading.defaultProps = {
  size: 'small',
}

Loading.propTypes = {
  size: PropTypes.string,
}

export default Loading
