import * as React from 'react'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'

const Input = React.forwardRef(
  (
    {
      value,
      onChangeText,
      placeholder,
      secureTextEntry,
      multiline,
      numberOfLines,
      autoCorrect,
    },
    ref
  ) => {
    return (
      <StyledInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        autoCorrect={autoCorrect}
        multiline={multiline}
        numberOfLines={numberOfLines}
        editable
        ref={ref}
      />
    )
  }
)

Input.defaultProps = {
  multiline: false,
  numberOfLines: 1,
  secureTextEntry: false,
  autoCorrect: false,
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  secureTextEntry: PropTypes.bool,
  autoCorrect: PropTypes.bool,
  multiline: PropTypes.bool,
  numberOfLines: PropTypes.number,
}

const StyledInput = styled.TextInput`
  border: 1px solid red;
  margin-bottom: 10px;
  height: 40px;
`

export default Input
