import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import axios from 'axios'

import LinkingConfiguration from './navigation/LinkingConfiguration'
import SignInScreen from './screens/SignInScreen'
import SignUpScreen from './screens/SignUpScreen'
import AuthContext from './services/context'
import authReducer from './reducers/authReducer'
import deviceStorage from './services/deviceStorage'
import useCachedResources from './hooks/useCachedResources'
import Loading from './components/Loading'
import BottomTabNavigator from './navigation/BottomTabNavigator'

const Stack = createStackNavigator()

export default function App() {
  const isLoadingComplete = useCachedResources()
  const [state, dispatch] = React.useReducer(authReducer, {
    isLoading: true,
    isSignout: false,
    userToken: null,
  })

  const authContext = React.useMemo(
    () => ({
      signIn: async (user) => {
        return axios
          .post('http://localhost:3000/login', user)
          .then(({ data }) => {
            deviceStorage.saveItem('userToken', data.token)
            dispatch({ type: 'SIGN_IN', token: data.token })
          })
          .catch((error) => {
            // eslint-disable-next-line no-console
            console.warn('error', error)
          })
      },
      signOut: () => {
        deviceStorage.deleteJWT()
        dispatch({ type: 'SIGN_OUT' })
      },
      signUp: async (user) => {
        return axios
          .post('http://localhost:3000/users', user)
          .then(({ data }) => {
            deviceStorage.saveItem('userToken', data.token)
            dispatch({ type: 'SIGN_UP', token: data.token })
          })
          .catch((error) => {
            // eslint-disable-next-line no-console
            console.warn('error', error)
          })
      },
    }),
    []
  )

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken

      try {
        userToken = await deviceStorage.loadJWT()
      } catch (e) {
        // Restoring token failed
      }

      dispatch({ type: 'RESTORE_TOKEN', token: userToken })
    }

    bootstrapAsync()
  }, [])

  if (!isLoadingComplete || state.isLoading) {
    return <Loading />
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer linking={LinkingConfiguration}>
        <Stack.Navigator>
          {state.userToken ? (
            <Stack.Screen name="Root" component={BottomTabNavigator} />
          ) : (
            <>
              <Stack.Screen name="SignIn" component={SignInScreen} />
              <Stack.Screen name="SignUp" component={SignUpScreen} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  )
}
