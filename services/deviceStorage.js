/* eslint-disable no-console */
import { AsyncStorage } from 'react-native'

const deviceStorage = {
  async saveItem(key, value) {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (error) {
      console.warn(`AsyncStorage Error: ${error.message}`)
    }
  },
  async loadJWT() {
    try {
      return await AsyncStorage.getItem('userToken')
    } catch (error) {
      return console.warn(`AsyncStorage Error: ${error.message}`)
    }
  },
  async deleteJWT() {
    try {
      await AsyncStorage.removeItem('userToken')
    } catch (error) {
      console.warn(`AsyncStorage Error: ${error.message}`)
    }
  },
}

export default deviceStorage
