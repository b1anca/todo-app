import * as React from 'react'
import { TextInput, Button } from 'react-native'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import AuthContext from '../services/context'

export default function SignInScreen({ navigation }) {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')

  const { signIn } = React.useContext(AuthContext)

  return (
    <Container>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button title="sign in" onPress={() => signIn({ username, password })} />
      <Button title="sign up" onPress={() => navigation.navigate('SignUp')} />
    </Container>
  )
}

SignInScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
`
