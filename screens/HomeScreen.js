import * as React from 'react'
import { Text, Button } from 'react-native'
import styled from 'styled-components/native'
import AuthContext from '../services/context'

export default function HomeScreen() {
  const { signOut } = React.useContext(AuthContext)

  return (
    <Container>
      <Text>home screen</Text>
      <Button title="sign out" onPress={signOut} />
    </Container>
  )
}

HomeScreen.navigationOptions = {
  header: null,
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`
