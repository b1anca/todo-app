import * as React from 'react'
import { Text, Button } from 'react-native'
import styled from 'styled-components/native'
import PropTypes from 'prop-types'
import Input from '../components/Input'
import AuthContext from '../services/context'

const SignUpScreen = ({ navigation }) => {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [passwordConfirmation, setPasswordConfirmation] = React.useState('')
  const { signUp } = React.useContext(AuthContext)

  return (
    <Container>
      <Text>SignUp</Text>
      <Input
        value={username}
        onChangeText={setUsername}
        editable
        placeholder="username"
      />
      <Input
        value={password}
        onChangeText={setPassword}
        editable
        placeholder="password"
        secureTextEntry
      />
      <Input
        value={passwordConfirmation}
        onChangeText={setPasswordConfirmation}
        editable
        placeholder="passwordConfirmation"
        secureTextEntry
      />

      <Button title="sign up" onPress={() => signUp({ username, password })} />
      <Button title="sign in" onPress={() => navigation.navigate('SignIn')} />
    </Container>
  )
}

const Container = styled.View`
  padding-horizontal: 10px;
  margin-top: 50px;
`

SignUpScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
}

export default SignUpScreen
