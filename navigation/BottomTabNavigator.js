import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import * as React from 'react'
import PropTypes from 'prop-types'

import TabBarIcon from '../components/TabBarIcon'
import HomeScreen from '../screens/HomeScreen'
import LinksScreen from '../screens/LinksScreen'

const BottomTab = createBottomTabNavigator()
const INITIAL_ROUTE_NAME = 'Home'

const getHeaderTitle = (route) => {
  const routeState = (route.state && route.state.routes[route.state.index]) || {}
  const routeName = routeState.name || INITIAL_ROUTE_NAME

  switch (routeName) {
    case 'Home':
      return 'How to get started'
    case 'Links':
      return 'Links to learn more'
    default:
      return 'aslkdfj'
  }
}

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  navigation.setOptions({ headerTitle: getHeaderTitle(route) })

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen name="Home" component={HomeScreen} />
      <BottomTab.Screen
        name="Links"
        component={LinksScreen}
        options={{
          title: 'Links',
          // eslint-disable-next-line react/prop-types
          tabBarIcon: ({ focused }) => (
            <TabBarIcon focused={focused} name="md-book" />
          ),
        }}
      />
    </BottomTab.Navigator>
  )
}

BottomTabNavigator.propTypes = {
  navigation: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
}
